﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroWarrior : Hero {

	public GameObject redSoldier;
	public GameObject greenSoldier;
	public GameObject whiteSoldier;
	public GameObject blueSoldier;
	public GameObject blackSoldier;
	public GameObject yellowSoldier;
	public GameObject crown;

	// Use this for initialization
	public override void Start () {
		base.Start ();
		moveSpeed = 5.0f;

		// position the different models with an offset so they don't collide
		blackSoldier.transform.position = new Vector3(-0.7f + 0 * 0.55f, 0, 0 * 0.55f);
		blueSoldier.transform.position = new Vector3(-0.7f + 1 * 0.55f, 0, 0 * 0.55f);
		whiteSoldier.transform.position = new Vector3(-0.7f + 2 * 0.55f, 0, 0 * 0.55f);
		redSoldier.transform.position = new Vector3(-0.7f + 0 * 0.55f, 0, 1 * 0.55f);
		yellowSoldier.transform.position = new Vector3(-0.7f + 1 * 0.55f, 0, 1 * 0.55f);
		greenSoldier.transform.position = new Vector3(-0.7f + 2 * 0.55f, 0, 1 * 0.55f);
		TurnCrownOff ();
	}
		
	// Update is called once per frame
	public override void Update () {
		base.Update ();
	}

	// crown
	public void TurnCrownOn() {
		blackSoldier.transform.Find ("crown").gameObject.SetActive (true);
		blueSoldier.transform.Find ("crown").gameObject.SetActive (true);
		redSoldier.transform.Find ("crown").gameObject.SetActive (true);
		yellowSoldier.transform.Find ("crown").gameObject.SetActive (true);
		greenSoldier.transform.Find ("crown").gameObject.SetActive (true);
		whiteSoldier.transform.Find ("crown").gameObject.SetActive (true);
	}

	public void TurnCrownOff() {
		blackSoldier.transform.Find ("crown").gameObject.SetActive (false);
		blueSoldier.transform.Find ("crown").gameObject.SetActive (false);
		redSoldier.transform.Find ("crown").gameObject.SetActive (false);
		yellowSoldier.transform.Find ("crown").gameObject.SetActive (false);
		greenSoldier.transform.Find ("crown").gameObject.SetActive (false);
		whiteSoldier.transform.Find ("crown").gameObject.SetActive (false);
	}

	// puts a crown on the hero's head
	public override void WearCrown (Crown crown) {
		TurnCrownOn ();
		base.WearCrown (crown);
	}

	// removes a crown from the hero's head
	public override void RemoveCrown (Crown crown) {
		TurnCrownOff ();
		base.RemoveCrown (crown);
	}

}
