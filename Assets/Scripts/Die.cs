﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Die : MonoBehaviour {

	public float forceStrength = 20.0f;
	public  float torque = 50.0f;
	public ForceMode forceMode;
	public Rigidbody dieRb;
	public LayerMask dieValueColliderLayer = -1;
	private int dieValue = 1;
	public enum enDieState {
		Stopped, Rolling
	}
	private enDieState dieSate = enDieState.Stopped;
	public delegate void RollDone (int dieValue);
	public RollDone rollDone;

	public int DieValue {
		get {
			return dieValue;
		}
	}

	public void TossDie () {
		// can't toss a die that is already rolling
		if (dieSate == enDieState.Rolling)
			return;

		// toss the die in the air
		dieRb.AddForce (new Vector3(0.1f, 1.0f, 0.1f) * forceStrength, forceMode);
		dieRb.AddTorque (Random.onUnitSphere * torque, forceMode);
		dieSate = enDieState.Rolling;
	}

	// Use this for initialization
	void Start () {
		dieRb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		// check if die finished rolling
		if (dieSate == enDieState.Rolling && dieRb.IsSleeping()) {
			dieSate = enDieState.Stopped;
			if (Physics.Raycast (transform.position, Vector3.up, out hit, Mathf.Infinity, dieValueColliderLayer)) {
				dieValue = hit.collider.GetComponent<DieValue> ().value;
				if (rollDone != null)
					rollDone (dieValue);
			} else {
				TossDie ();
			}
		}
	}
}
