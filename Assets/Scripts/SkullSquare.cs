﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullSquare : GameSquare {

	// Constructor for StartSquare
	public SkullSquare () {

	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	// a handler to when a hero lands and stops on this square
	public override List<GameEvents> LandOnSquare (Hero hero) {
		List<GameEvents> newGameEvents = new List<GameEvents> ();
		newGameEvents.AddRange (base.LandOnSquare (hero));
		newGameEvents.Add (GameEvents.SkullEvent);
		return newGameEvents;
	}

	// Destructor for StartSquare
	~SkullSquare () {

	}
}