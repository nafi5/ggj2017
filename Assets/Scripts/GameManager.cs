﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public Hero gameHero;
	public BoardManager boardManager;
	public EventManager eventManager;
	public SmoothFollow cameraFollow;
	public List<Hero> gameHeroes;
	private int selectedHero;
	public DieArea dieAreaPrefab;
	public DieArea dieArea;
	public Crown crownPrefab;
	public Crown gameCrown;
	public string buttonName = "Jump";

	public StatusManager statusManager;

	public enum enDirection {
		Left, Right, Top, Bottom
	}

	private void CameraFocusHero (Hero hero) {
		cameraFollow.target = hero.transform;
	}

	// change the given GameObject color
	public void ChangeGameObjectColor(GameObject go, float r, float g, float b){
		Color col = new Color(r, g, b, 1);
		MeshRenderer gameObjectRenderer = go.GetComponent<MeshRenderer>();
		gameObjectRenderer.material.color = col;
	}

	// change the RollerBall color of the HeroWarrior object to according
	// to the given color
	private void PaintWarriorHeroColor (HeroWarrior heroWarrior, PlayerColor pColor) {
		GameObject rollerBall = heroWarrior.transform.Find ("RollerBall").gameObject as GameObject;

		heroWarrior.blackSoldier.SetActive (false);
		heroWarrior.whiteSoldier.SetActive (false);
		heroWarrior.blueSoldier.SetActive (false);
		heroWarrior.redSoldier.SetActive (false);
		heroWarrior.yellowSoldier.SetActive (false);
		heroWarrior.greenSoldier.SetActive (false);

		switch (pColor) {
		case PlayerColor.PColorGreen:
			ChangeGameObjectColor (rollerBall, 0f, 1f, 0f);
			heroWarrior.greenSoldier.SetActive (true);
			break;
		case PlayerColor.PColorRed:
			ChangeGameObjectColor (rollerBall, 1f, 0f, 0f);
			heroWarrior.redSoldier.SetActive (true);

			break;
		case PlayerColor.PColorYellow:
			ChangeGameObjectColor (rollerBall, 1f, 1f, 0.5f);
			heroWarrior.yellowSoldier.SetActive (true);

			break;
		case PlayerColor.PColorBlue:
			ChangeGameObjectColor (rollerBall, 0f, 0f, 1f);
			heroWarrior.blueSoldier.SetActive (true);

			break;
		case PlayerColor.PColorWhite:
			ChangeGameObjectColor (rollerBall, 1f, 1f, 1f);
			heroWarrior.whiteSoldier.SetActive (true);

			break;
		case PlayerColor.PColorBlack:
			ChangeGameObjectColor (rollerBall, 0f, 0f, 0f);
			heroWarrior.blackSoldier.SetActive (true);

			break;
		}
	}

	// spawns a new hero on the given gameSquare
	private Hero spawnHero (GameSquare gameSquare, PlayerColor pColor=PlayerColor.PColorBlack) {
		Hero newGameHero;
		newGameHero = Instantiate (gameHero, new Vector3 (0, 0, 0), Quaternion.identity) as Hero;
		newGameHero.SetSquare (gameSquare);
		newGameHero.enteredSquare = HandleArrivalAtSquare;
		newGameHero.statesChanged = HandleChangeInStates;
		newGameHero.playerColor = pColor;
		PaintWarriorHeroColor ((newGameHero as HeroWarrior), pColor);
		HandleChangeInStates (newGameHero);
		gameHeroes.Add (newGameHero);
		newGameHero.heroIndex = gameHeroes.Count - 1;

		return newGameHero;
	}

	// returns the position where the die area should be located
	// compared to the hero's position
	private Vector3 GetDieAreaPos (Hero hero) {
		Vector3 pos = hero.transform.position;
		pos.z -= 2.0f;
		return pos;
	}

	// init a new die area next to the hero
	private void InitDieArea () {
		dieArea = Instantiate (dieAreaPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as DieArea;
		dieArea.AddDie ();
		dieArea.AddDie ();
		dieArea.rollDone = HandleDieRollDone;
	}

	// init the crown in the game
	private void InitCrown () {
		gameCrown = Instantiate (crownPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as Crown;
	}

	// shows the Die Area of the Hero
	public void ShowDieArea (Hero hero) {
		dieArea.transform.position = GetDieAreaPos(hero);
	}

	// advances the turn to the next hero
	private int NextTurn () {
		// unset the current player hero turn
		UnsetTurnTo(gameHeroes [selectedHero]);

		// advance the turn
		selectedHero++;
		if (selectedHero >= gameHeroes.Count) {
			selectedHero = 0;
		}

		// set the turn to the next player hero
		SetTurnTo(gameHeroes [selectedHero]);

		// add the die roll handler back
		dieArea.rollDone += HandleDieRollDone;

		return selectedHero;
	}
		
	private void HandleEventDone (Hero hero, GameSquare gameSquare) {
		if (hero.HasMoreStepsToMove ()) {
			hero.MoveToNextSquare ();
		} else {
			NextTurn ();
		}
	}

	private void HandleArrivalAtSquare (Hero hero, GameSquare gameSquare) {
		List<GameEvents> newGameEvents = new List<GameEvents> ();
		if (hero.HasMoreStepsToMove ()) {
			gameSquare.StepOnSquare (hero);
			hero.MoveToNextSquare ();
		} else {
			newGameEvents.AddRange(gameSquare.LandOnSquare (hero));
			if (newGameEvents.Count > 0) {
				eventManager.AddGameEvents (newGameEvents);
				eventManager.eventsDone = HandleEventDone;
				eventManager.SetHeroAndSquare (hero, gameSquare);
				eventManager.TriggerAllEvents ();
				return;
			}
			NextTurn ();
		}
	}

	public void HandleChangeInStates(Hero hero){
		statusManager.SetStatus (hero.playerColor, hero.points, hero.health);// (hero.playerColor, hero.points, hero.health);
	}

	private void HandleDieRollDone (List<int> tossResults) {
		dieArea.rollDone -= HandleDieRollDone;
		int diceSum = 0;
		foreach (int tossResult in tossResults) {
			diceSum += tossResult;
		}
		gameHeroes [selectedHero].CurrentSquare.LeaveSquare (gameHeroes [selectedHero]);
		gameHeroes [selectedHero].MoveNumSteps (diceSum);
	}

	// Unset the turn to the selected player
	public void UnsetTurnTo (Hero unselectedHero) {
		// does nothing at the moment
	}

	// Set the turn to the selected player
	public void SetTurnTo (Hero selectedHero) {
		CameraFocusHero(selectedHero);
		ShowDieArea (selectedHero);
		statusManager.notify ("Press space bar to roll the dice");
	}

	// Start a new game with the given number of players
	public void GameStart (int numberOfPlayers, int boardSize) {
		boardManager.BuildGameBoard (boardSize);

		// Create the hero characters of the players
		for (int i = 0; i < numberOfPlayers; i++) {
			Hero newHero = spawnHero (boardManager.GetStartSquare(), (PlayerColor)i);
		}

		// Init the area where the dice are thrown
		InitDieArea ();

		// Set the turn for the first player
		selectedHero = 0;
		SetTurnTo (gameHeroes [selectedHero]);

		InitCrown ();
		gameCrown.SetSquare (boardManager.GetSkullSquare(2));
	}

	public GameManager () {
		gameHeroes = new List<Hero> ();
	}

	// Use this for initialization
	void Start () {
		GameStart (6, 8);
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown (buttonName)) {
			dieArea.TossDice();
		}
	}

	~GameManager () {
		gameHeroes.Clear ();
	}
}
