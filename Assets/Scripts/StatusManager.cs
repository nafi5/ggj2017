﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusManager : MonoBehaviour {

	public UnityEngine.UI.Text redStatus, whiteStatus, greenStatus, yellowStatus, blueStatus, blackStatus, notifications;


	// Use this for initialization
	void Awake () {
		
	}

	public void notify (string note) {
		notifications.text = note;
	}
	
	public void SetStatus(PlayerColor pc, int points, int lives){
		switch (pc) {
		case PlayerColor.PColorBlack:
			blackStatus.text =  "Black:   " + lives + "     " + points;
			break;
		case PlayerColor.PColorBlue:
			blueStatus.text =   "Blue:   " + lives + "     " + points;
			break;
		case PlayerColor.PColorRed:
			redStatus.text =    "Red:   " + lives + "     " + points;
			break;
		case PlayerColor.PColorYellow:
			yellowStatus.text = "Yellow:   " + lives + "     " + points;
			break;
		case PlayerColor.PColorWhite:
			whiteStatus.text =  "White:   " + lives + "     " + points;
			break;
		case PlayerColor.PColorGreen:
			greenStatus.text =  "Green:   " + lives + "     " + points;
			break;
		}

	}

	public void toggleStatusText(PlayerColor pc, bool turnOn){
		switch (pc) {
		case PlayerColor.PColorBlack:
			blackStatus.enabled = turnOn; 
			break;
		case PlayerColor.PColorBlue:
			blueStatus.enabled = turnOn; 
			break;
		case PlayerColor.PColorRed:
			redStatus.enabled = turnOn; 			
			break;
		case PlayerColor.PColorYellow:
			yellowStatus.enabled = turnOn; 			
			break;
		case PlayerColor.PColorWhite:
			whiteStatus.enabled = turnOn; 			
			break;
		case PlayerColor.PColorGreen:
			greenStatus.enabled = turnOn; 			
			break;
		}
	}
}

public enum PlayerColor
{
	PColorRed,
	PColorWhite,
	PColorGreen,
	PColorBlue,
	PColorBlack,
	PColorYellow
}