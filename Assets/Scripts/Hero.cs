﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : MonoBehaviour {

	public int heroIndex;
	private const int START_HEALTH = 5;
	private GameSquare prevSquare, currentSquare;
	private enHeroState heroState;
	protected float moveSpeed;
	private float startMoveTime;
	private int stepsTaken;
	private int stepsLeftToMove = 0;
	protected Crown heroCrown;

	public delegate void EnteredSquare(Hero hero, GameSquare gameSquare);
	public EnteredSquare enteredSquare;

	// an event that triggers if either health or points are changed
	public delegate void StatesChanged(Hero hero);
	public StatesChanged statesChanged;

	// an event that triggers when the hero's health reaches 0
	public delegate void HeroDied(Hero hero);
	public HeroDied heroDied;

	public PlayerColor playerColor{ get; set;}
	public int health{ get; set;}
	public int points{ get; set;}

	public GameSquare CurrentSquare {
		get {
			return currentSquare;
		}
	}

	public Crown HeroCrown {
		get {
			return heroCrown;
		}
	}

	public enum enHeroState {
		Stopped, Walking
	}

	private void enterSquare () {
		if (enteredSquare != null)
			enteredSquare (this, currentSquare);
	}

	// moves hero from prevSquare to the currentSquare in a smooth
	// lerp motion
	private void MoveHero () {
		float distCovered = (Time.time - startMoveTime) * moveSpeed;
		float moveTotalDist = Vector3.Distance (prevSquare.transform.position,
			                      currentSquare.transform.position);
		float fracJourney = distCovered / moveTotalDist;
		transform.position = Vector3.Lerp (prevSquare.transform.position,
			currentSquare.transform.position, fracJourney);
		if (fracJourney >= 1.0) {
			heroState = enHeroState.Stopped;
			if (enteredSquare != null)
				Invoke ("enterSquare", 0.1f);
		}
	}

	public int StepsTaken {
		get {
			return stepsTaken;
		}
	}

	// reduces health to the hero and returns true if the hero died
	public void ReduceHealth (int amount) {
		health -= amount;
		Debug.Log ("hero's health is now " + health);//////
		if(statesChanged != null)
			statesChanged (this);


		if (health < 0) {
			health = 0;
			if (heroDied != null)
				heroDied (this);
		}
	}

	// a constructor for the Hero class
	public Hero () {
		currentSquare = null;
		prevSquare = null;
		heroState = enHeroState.Stopped;
		startMoveTime = 0.0f;
		moveSpeed = 2.0f;
		// the default color is black but this is changed in the GameManager later
		playerColor = PlayerColor.PColorBlack;
		health = START_HEALTH;
		points = 0;
		heroCrown = null;
	}

	// set the current square the hero is standing on and teleport
	// the hero to it
	public virtual void SetSquare (GameSquare square) {
		if (heroState != enHeroState.Stopped) {
			Debug.Log ("can't set hero square when it is not stopped");
		}
		prevSquare = currentSquare;
		currentSquare = square;
		transform.position = square.transform.position;
		if (enteredSquare != null)
			Invoke ("enterSquare", 0.5f);
	}

	// move the hero to a new square and make the hero start moving
	// to the new square
	public virtual void MoveToSquare (GameSquare square) {
		if (currentSquare == null) {
			Debug.Log ("can't move hero before setting an initial square");
			return;
		}
		stepsTaken += 1;
		prevSquare = currentSquare;
		currentSquare = square;
		startMoveTime = Time.time;
		heroState = enHeroState.Walking;
	}

	// move the hero to the next square on the board
	public void MoveToNextSquare () {
		stepsLeftToMove--;
		MoveToSquare (currentSquare.nextSquares [0]);
	}

	public bool HasMoreStepsToMove () {
		if (stepsLeftToMove > 0) {
			return true;
		}
		return false;
	}

	// puts a crown on the hero's head
	public virtual void WearCrown (Crown crown) {
		heroCrown = crown;
	}

	// removes a crown from the hero's head
	public virtual void RemoveCrown (Crown crown) {
		heroCrown = null;
	}

	// returns true if this hero wears a crown and false otherwise
	public bool HasCrown () {
		if (heroCrown != null)
			return true;
		return false;
	}

	// move the hero the specified amount of steps
	public void MoveNumSteps (int stepsToMove) {
		stepsLeftToMove = stepsToMove;
		MoveToNextSquare ();
	}

	// Use this for initialization
	public virtual void Start () {

	}
	
	// Update is called once per frame
	public virtual void Update () {
		if (heroState == enHeroState.Walking)
			MoveHero ();
	}

}
