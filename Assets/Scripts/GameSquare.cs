﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSquare : MonoBehaviour {

	protected const float WIDTH = 2.0f;
	public GameSquare prevSquare;
	public List<GameSquare> nextSquares;
	public List<Hero> gameHeroesOnSquare;
	public Renderer planeRenderer;
	public Crown crown;

	public float width { get {return WIDTH; } }

	// Constructor for GameSquare
	public GameSquare () {
		nextSquares = new List<GameSquare>();
		gameHeroesOnSquare = new List<Hero> ();
	}

	public void AddNextSquare(GameSquare next) {
		nextSquares.Add (next);
	}

	public string ToKey() {
		int x = Mathf.RoundToInt (transform.position.x);
		int z = Mathf.RoundToInt (transform.position.z);
		return (x.ToString () + "," + z.ToString ());
	}

	// a handler to when a hero steps on this square but doesn't
	// stop on it
	public virtual void StepOnSquare (Hero hero) {
		if (crown != null) {
			crown.SetHero (hero);
			// SetHero sets the square's crown to null but it doesn't work
			// here since we are using the crown to call this function
			crown = null;
		}
	}

	// a handler to when a hero lands and stops on this square
	public virtual List<GameEvents> LandOnSquare (Hero hero) {
		List<GameEvents> newGameEvents = new List<GameEvents> ();
		if (crown != null) {
			crown.SetHero (hero);
			crown = null;
		}
		foreach (Hero currHero in gameHeroesOnSquare) {
			// if the hero has a crown, drop his crown on this square
			if (currHero.HasCrown ()) {
				currHero.HeroCrown.SetSquare (this);
			} else {
				// if no crown, hit this hero for 1 damage
				currHero.ReduceHealth (1);
			}
		}
		gameHeroesOnSquare.Add (hero);
		return newGameEvents;
	}

	// a handler to when a hero teleports to this square
	public virtual void TeleportedToSquare (Hero hero) {
		gameHeroesOnSquare.Add (hero);
	}

	// a handler to when a hero leaves this square
	public virtual void LeaveSquare (Hero hero) {
		gameHeroesOnSquare.Remove (hero);
	}

	// Use this for initialization
	void Start () {
		crown = null;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// Destructor for GameSquare
	~GameSquare () {

	}
}
