﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crown : MonoBehaviour {

	GameSquare currentSquare;
	Hero currentHero;

	// Use this for initialization
	void Start () {
		currentSquare = null;
		currentHero = null;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// places the trasform position of the crown on top of a hero
	private void PositionCrownOnHero (Hero hero) {
		Vector3 heroPos = hero.transform.position;
		Vector3 newPos = new Vector3 (heroPos.x, heroPos.y, heroPos.z);
		transform.position = newPos;
	}

	// places the transform position of the crown on a square
	private void PositionCrownOnSquare (GameSquare square) {
		transform.position = square.transform.position;
	}

	// move the crwon to be on a square
	public void SetSquare (GameSquare square) {
		// remove the crown from the square if it was already
		// placed on another square
		if (currentSquare != null)
			currentSquare.crown = null;
		currentSquare = square;
		currentSquare.crown = this;
		// remove the crown from a hero if a hero had this crown
		if (currentHero != null) {
			currentHero.RemoveCrown (this);
			currentHero = null;
		}
		gameObject.SetActive (true);
		PositionCrownOnSquare (square);
	}

	// put the crown on the hero's head
	public void SetHero (Hero hero) {
		// remove the crown from a square if it was placed on a square
		// before it was set on the hero
		if (currentSquare != null) {
			currentSquare.crown = null;
			currentSquare = null;
		}
		// make the selected hero wear the crown
		currentHero = hero;
		currentHero.WearCrown (this);
		gameObject.SetActive (false);
		PositionCrownOnHero (hero);
	}
}
