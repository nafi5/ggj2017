﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSquare : GameSquare {

	public int value1, value2;

	// Constructor for StartSquare
	public StepSquare () {

	}

	// a handler to when a hero steps on this square but doesn't
	// stop on it
	public override void StepOnSquare (Hero hero) {
		base.StepOnSquare (hero);
	}

	// a handler to when a hero lands and stops on this square
	public override List<GameEvents> LandOnSquare (Hero hero) {
		return base.LandOnSquare (hero);
	}

	// a handler to when a hero leaves this square
	public override void LeaveSquare (Hero hero) {
		base.LeaveSquare (hero);
	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	// Destructor for StartSquare
	~StepSquare () {

	}
}