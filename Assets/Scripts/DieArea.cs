﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieArea : MonoBehaviour {

	public Die diePrefab;
	private List<Die> diceList;
	private List<int> tossResults;
	private int diceSum;
	private int diceRolled;

	public delegate void RollDone (List<int> tossResults);
	public RollDone rollDone;

	//a handler for each dice that finished rolling in the die area
	private void dieAreaRollDone (int dieValue) {
		tossResults.Add (dieValue);
		diceRolled += 1;
		if (diceRolled == diceList.Count) {
			if (rollDone != null) {
				rollDone (tossResults);
				tossResults.Clear ();
			}
		}
	}

	public void AddDie () {
		Die newDie = Instantiate (diePrefab, transform.position, Quaternion.identity);
		diceList.Add (newDie);
		newDie.transform.parent = transform;
	}

	public Die GetDie (int index) {
		return diceList [index];
	}

	// toss all dice in the die area at once
	public void TossDice () {
		diceSum = 0;
		diceRolled = 0;
		foreach (Die die in diceList) {
			die.rollDone = dieAreaRollDone;
			die.TossDie ();
		}
	}

	public DieArea () {
		diceList = new List<Die> ();
		tossResults = new List<int> ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
