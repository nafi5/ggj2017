﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour {

	// all square types
	public GameSquare gameSquarePrefab;
	public StartSquare startSquarePrefab;
	public SkullSquare skullSquarePrefab;
	public StepSquare stepSquarePrefab;
	public EMSquare eMSquarePrefab;
	public QMSquare qMSquarePrefab;

	private Dictionary<string, GameSquare> gameSquares;
	private List<GameSquare> endSquares;
	private int boardDepth;
	private GameSquare startSquare;
	private const float SPLIT_CHANCE = 0.2f;

	// special square lists
	private List<SkullSquare> skullSquares;

	static System.Random random = new System.Random();

	// for steps
	private int stepV1 = 1, stepV2 = 1;

	public enum enDirection {
		Left, Right, Top, Bottom
	}

	public enum enSquare
	{
		EmptySquare, StartSquare, SkullSquare, StepSquare, EMSquare, QMSquare
	}

	// returns a skull square with the given index
	public SkullSquare GetSkullSquare (int index) {
		if (index >= skullSquares.Count) {
			throw new UnityException ("Skull index " + index + " is not valid!!");
		}
		return skullSquares [index];
	}

	// instansiate a new square of the given <squareType>
	private GameSquare GetSquareByType (enSquare squareType) {
		switch (squareType) {
			case enSquare.EmptySquare:
			{
				return Instantiate (gameSquarePrefab, new Vector3 (0, 0, 0),
					Quaternion.identity) as GameSquare;
			}
			case enSquare.StartSquare:
			{
				return Instantiate (startSquarePrefab, new Vector3 (0, 0, 0),
					Quaternion.identity) as GameSquare;
			}
			case enSquare.SkullSquare:
			{
				return Instantiate (skullSquarePrefab, new Vector3 (0, 0, 0),
					Quaternion.identity) as GameSquare;
			}
			case enSquare.StepSquare:
			{
				return Instantiate (stepSquarePrefab, new Vector3 (0, 0, 0),
					Quaternion.identity) as GameSquare;
			}
			case enSquare.EMSquare:
			{
				return Instantiate (eMSquarePrefab, new Vector3 (0, 0, 0),
					Quaternion.identity) as GameSquare;
			}
			case enSquare.QMSquare:
			{
				return Instantiate (qMSquarePrefab, new Vector3 (0, 0, 0),
					Quaternion.identity) as GameSquare;
			}
		}
		// return null if an unknown square type is given
		return null;
	}

	private void SetupGameSquareByType (GameSquare newSquare, enSquare squareType) {
		//  handle only step squares
		if (squareType == enSquare.StepSquare) {
			if (stepV1 == 6 && stepV2 > 6) {
				throw new UnityException ("Error: Step Square more than 6 6 !!!!");////////
			}

			// change texture on the game square
			Texture newSquareTexture = (Texture)Resources.Load ("step_" + stepV1 + stepV2);
			newSquare.planeRenderer.material.mainTexture = newSquareTexture;

			(newSquare as StepSquare).value1 = stepV1;
			(newSquare as StepSquare).value2 = stepV2;
			if (stepV2 == 6 && stepV1 < 6) {
				stepV1++;
				stepV2 = stepV1;
			} else if (stepV2 < 6) {
				stepV2++;
			}
		} else if (squareType == enSquare.SkullSquare) {
			// change texture on the game square
			string texture_name = "skull0" + (skullSquares.Count + 1);
			Texture newSquareTexture = (Texture)Resources.Load (texture_name);
			newSquare.planeRenderer.material.mainTexture = newSquareTexture;
			skullSquares.Add (newSquare as SkullSquare);
		}
	}

	// spawns a new square in the game
	// parentSquare is the parent of the new square
	// direction is the direction to spawn the new square relative to the parent
	private GameSquare SpawnSquare(GameSquare parentSquare, enSquare squareType,
								   enDirection direction) {
		float zDiff = 0.0f, xDiff = 0.0f;
		GameSquare newSquare;
		newSquare = GetSquareByType (squareType);
		SetupGameSquareByType (newSquare, squareType);
		if (parentSquare != null) {
			switch (direction) {
				case enDirection.Left:
				{
					xDiff -= parentSquare.width;
					break;
				}
				case enDirection.Right:
				{
					xDiff += parentSquare.width;
					break;
				}
				case enDirection.Bottom:
				{
					zDiff -= parentSquare.width;
					break;
				}
				case enDirection.Top:
				{
					zDiff += parentSquare.width;
					break;
				}
			}
			newSquare.transform.position = new Vector3 (parentSquare.transform.position.x + xDiff,
				parentSquare.transform.position.y, parentSquare.transform.position.z + zDiff);
			parentSquare.AddNextSquare (newSquare);
		}
		gameSquares.Add (newSquare.ToKey(), newSquare);
		return newSquare;
	}

	// Init the board by adding the first square
	private void InitBoard (enSquare squareTypeFirst) {
		boardDepth = 0;
		startSquare = SpawnSquare (null, squareTypeFirst, enDirection.Left);
		endSquares.Add (startSquare);
	}

	// this function gets a float chance percentage to get true
	// and via a random function, reutrns true with chance <chance>
	// and false with chance <1 - chance>
	private bool ChanceToOutcome(float chance) {
		int intChance = Mathf.RoundToInt (chance * 100) - 1;
		if (random.Next (100) <= intChance)
			return true;
		return false;
	}

	// convert a game square position to a string key
	private string PosToKey (float xf, float zf) {
		int x = Mathf.RoundToInt (xf);
		int z = Mathf.RoundToInt (zf);
		return (x.ToString () + "," + z.ToString ());
	}

	// returns true if a square can be positioned in slot <xf, zf> on the left of a parent
	// <width> is the width of each square.
	private bool IsLeftFree (float xf, float zf, float width) {
		string current = PosToKey (xf, zf);
		string top = PosToKey (xf, zf + width);
		string topRight =  PosToKey (xf + width, zf + width);
		string bottom = PosToKey (xf, zf - width);
		string bottomRight =  PosToKey (xf + width, zf - width);
		if (gameSquares.ContainsKey (current) ||
			(gameSquares.ContainsKey (top) && gameSquares.ContainsKey (topRight)) ||
			(gameSquares.ContainsKey (bottom) && gameSquares.ContainsKey (bottomRight)))
			return false;
		return true;
	}

	// returns true if a square can be positioned in slot <xf, zf> on the top of a parent
	// <width> is the width of each square.
	private bool IsTopFree (float xf, float zf, float width) {
		string current = PosToKey (xf, zf);
		string left = PosToKey (xf - width, zf);
		string bottomLeft =  PosToKey (xf - width, zf - width);
		string right = PosToKey (xf + width, zf);
		string bottomRight =  PosToKey (xf + width, zf - width);
		if (gameSquares.ContainsKey (current) ||
			(gameSquares.ContainsKey (left) && gameSquares.ContainsKey (bottomLeft)) ||
			(gameSquares.ContainsKey (right) && gameSquares.ContainsKey (bottomRight)))
			return false;
		return true;
	}

	// returns true if a square can be positioned in slot <xf, zf> on the bottom of a parent
	// <width> is the width of each square.
	private bool IsBottomFree (float xf, float zf, float width) {
		string current = PosToKey (xf, zf);
		string left = PosToKey (xf - width, zf);
		string topLeft =  PosToKey (xf - width, zf + width);
		string right = PosToKey (xf + width, zf);
		string topRight =  PosToKey (xf + width, zf + width);
		if (gameSquares.ContainsKey (current) ||
			(gameSquares.ContainsKey (left) && gameSquares.ContainsKey (topLeft)) ||
			(gameSquares.ContainsKey (right) && gameSquares.ContainsKey (topRight)))
			return false;
		return true;
	}

	private void SetNextSquare (GameSquare prevSquare, int endIndex) {
		float x = prevSquare.transform.position.x;
		float z = prevSquare.transform.position.z;
		float width = prevSquare.width;

		// check if we connected to another route
		// and if so then merge with it
		string left = PosToKey (x - width, z);
		string top = PosToKey (x, z + width);
		string bottom = PosToKey (x, z - width);
		if (gameSquares.ContainsKey (left)) {
			prevSquare.AddNextSquare (gameSquares [left]);
			endSquares.RemoveAt (endIndex);
			return;
		}
		if (gameSquares.ContainsKey (top) &&
			prevSquare.prevSquare != null &&
			top != prevSquare.prevSquare.ToKey () &&
			!gameSquares[top].nextSquares.Contains(prevSquare)) {
			prevSquare.AddNextSquare (gameSquares [top]);
			endSquares.RemoveAt (endIndex);
			return;
		}
		if (gameSquares.ContainsKey (bottom) &&
			prevSquare.prevSquare != null &&
			bottom != prevSquare.prevSquare.ToKey () &&
			!gameSquares[bottom].nextSquares.Contains(prevSquare)) {
			prevSquare.AddNextSquare (gameSquares [bottom]);
			endSquares.RemoveAt (endIndex);
			return;
		}

		List<enDirection> dirOptions = new List<enDirection> ();
		// check which sides are available to continue the route
		if (IsLeftFree (x - width, z, width))
			dirOptions.Add (enDirection.Left);
		if (IsTopFree (x, z + width, width))
			dirOptions.Add (enDirection.Top);
		if (IsBottomFree (x, z - width, width))
			dirOptions.Add (enDirection.Bottom);

		while (dirOptions.Count != 0) {
			int randMove = random.Next (dirOptions.Count);
			enDirection nextDir = dirOptions[randMove];
			dirOptions.RemoveAt (randMove);
			GameSquare newSquare = SpawnSquare (prevSquare, enSquare.EmptySquare, nextDir);
			if (ChanceToOutcome (SPLIT_CHANCE)) {
				endSquares.Add (newSquare);
				continue;
			} else {
				endSquares [endIndex] = newSquare;
				return;
			}
		}
	}

	// Rules:
	// 1) road build only to left, top and bottom.
	// 2) if before we add another square, the parent square has more than one square
	//    next to it, make the next square the one that is not
	//    already its previous parent square or a square to the right.
	//    (remove it from the heads list)
	//    Also, make sure this new head did not already set us as the new head.
	// 3) The new position must not:
	//    a. contain a square
	//    b. if we continued left, must not have squares in (top, top-right), (bottom, bottom-right)
	//    c. if we continued top, must not have squares in (left, bottom-left), (right, bottom-right)
	//    d. if we continued bottom, must not have squares in (left, top-left), (right, top-right)
	// 4) road may split with a probability and building new squares
	//    will continue from each node
	public void BuildRandomBoard (int depth) {
		while (depth > 0) {
			// work on a copy of the endSquares list because it might change
			List<GameSquare> copyList = new List<GameSquare> (endSquares);
			for (int i = copyList.Count - 1; i >= 0; i--) {
				SetNextSquare (copyList [i], i);
			}
			depth -= 1;
			boardDepth += 1;
		}
	}

	public int BoardDepth {
		get {
			return boardDepth;
		}
	}

	// Gets a direction and returns the next direction on turning right
	private enDirection TurnRight (enDirection direction) {
		switch (direction) {
			case enDirection.Left:
			{
				return enDirection.Top;
			}
			case enDirection.Top:
			{
				return enDirection.Right;
			}
			case enDirection.Right:
			{
				return enDirection.Bottom;
			}
			default:
			{
				return enDirection.Left;
			}
		}
	}

	// Gets a direction and returns the next direction on turning left
	private enDirection TurnLeft (enDirection direction) {
		switch (direction) {
		case enDirection.Left:
			{
				return enDirection.Bottom;
			}
		case enDirection.Bottom:
			{
				return enDirection.Right;
			}
		case enDirection.Right:
			{
				return enDirection.Top;
			}
		default:
			{
				return enDirection.Left;
			}
		}
	}

	// Builds a square board when each side is the size of <length>.
	// The board will have squares taken from the squareTypeList
	public void BuildSquareBoard (int length, List<enSquare> squareTypeList) {
		int numOfSquares = (length * 4) - 4;
		int typeIndex = 0;
		enDirection currDirection = enDirection.Left;
		int rotation = 0;
		bool bFirstSquare = true;

		// init the board and set the start square
		InitBoard (squareTypeList[typeIndex]);
		typeIndex++;

		// add the rest of the squares
		while (numOfSquares > 1) {
			// make rotate the square when needed and not on the first square
			if ((numOfSquares % (length - 1) == 1) && (bFirstSquare == false)) {
				rotation++;
			}			
			endSquares [0] = SpawnSquare (endSquares [0], squareTypeList [typeIndex], currDirection);
			endSquares [0].transform.Rotate (Vector3.up * rotation * 90);

			numOfSquares--;
			typeIndex++;
			// loop on the type of squares if needed
			if (typeIndex >= squareTypeList.Count) {
				typeIndex = 0;
			}
			if (numOfSquares % (length - 1) == 0) {
				currDirection = TurnRight(currDirection);
			}
			bFirstSquare = false;
		}

		// connect the end square with the start square
		endSquares [0].AddNextSquare (GetStartSquare ());
	}

	// Builds a game board using the square board function
	public void BuildGameBoard (int length) {
		// Assuming length is 8 !!!!
		List<enSquare> squareTypeList = new List<enSquare>{
			enSquare.SkullSquare, enSquare.StepSquare,enSquare.StepSquare,enSquare.StepSquare, enSquare.QMSquare, enSquare.StepSquare,enSquare.StepSquare,
			enSquare.SkullSquare, enSquare.StepSquare,enSquare.StepSquare, enSquare.StepSquare, enSquare.EMSquare, enSquare.StepSquare, enSquare.StepSquare,
			enSquare.SkullSquare, enSquare.StepSquare,enSquare.StepSquare, enSquare.QMSquare, enSquare.StepSquare, enSquare.StepSquare, enSquare.StepSquare,
			enSquare.SkullSquare, enSquare.StepSquare,enSquare.StepSquare, enSquare.StepSquare, enSquare.StepSquare, enSquare.StepSquare, enSquare.StepSquare
		};
		BuildSquareBoard (length, squareTypeList);
	}

	public GameSquare GetStartSquare() {
		return startSquare;
	}

	public BoardManager () {
		gameSquares = new Dictionary<string, GameSquare> ();
		endSquares = new List<GameSquare> ();
		skullSquares = new List<SkullSquare> ();
	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	~BoardManager () {
		gameSquares.Clear ();
	}
}
