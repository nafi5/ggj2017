﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

	public GameManager gameManager;
	private List<GameEvents> gameEvents;
	private GameSquare currentGameSquare;
	private Hero currentHero;

	// an event that triggers when an event from the gameEvents is complete
	public delegate void EventsDone (Hero hero, GameSquare gameSquare);
	public EventsDone eventsDone;

	public EventManager () {
		gameEvents = new List<GameEvents> ();
		currentHero = null;
		currentGameSquare = null;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetHeroAndSquare (Hero hero, GameSquare gameSquare) {
		currentHero = hero;
		currentGameSquare = gameSquare;
	}

	public void AddGameEvents (List<GameEvents> newGameEvents) {
		gameEvents.AddRange (newGameEvents);
	}

	private void TriggerSingleEvent (GameEvents gameEvent) { 
		switch (gameEvent) {
		case GameEvents.SkullEvent:
			SkullEvent ();
			break;
		}
	}

	public void TriggerAllEvents () {
		if (gameEvents.Count < 0)
			return;
		TriggerSingleEvent (gameEvents [0]);
		gameEvents.RemoveAt (0);
	}

	private void HandleSkullRollDone (List<int> tossResults) {
		gameManager.dieArea.rollDone -= HandleSkullRollDone;
		int diceSum = 0;
		foreach (int tossResult in tossResults) {
			diceSum += tossResult;
		}
		Debug.Log ("You rolled dice in order to kill");

		foreach (Hero tempHero in gameManager.gameHeroes) {
			if (tempHero.CurrentSquare is StepSquare) {
				StepSquare stepSquare = tempHero.CurrentSquare as StepSquare;
				int squareSum = stepSquare.value1 + stepSquare.value2;
				if (diceSum == squareSum) {
					if (tempHero.HasCrown ()) {
						tempHero.HeroCrown.SetSquare (tempHero.CurrentSquare);
					} else {
						gameManager.statusManager.notify ("a hero has been hit by skull roll!");
						tempHero.ReduceHealth (1);
					}
				}
			}
		}

		// finish the event for now
		if (eventsDone != null)
			eventsDone(currentHero, currentGameSquare);
	}

	private void SkullEvent () {
		// Skull events are not available to heroes with crowns
		if (currentHero.HasCrown ()) {
			currentHero.points++;
			gameManager.HandleChangeInStates (currentHero);
			if (eventsDone != null)
				eventsDone(currentHero, currentGameSquare);
			return;
		}
		gameManager.statusManager.notify ("roll the dice again to hit your enemies");
		gameManager.dieArea.rollDone += HandleSkullRollDone;
		gameManager.ShowDieArea (currentHero);
	}
}

public enum GameEvents
{
	SkullEvent,
}